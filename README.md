Mirrors Documentation
====
This document is intended for Server Programmers who will be participating in the production of Mirrors code during their ramp up to the project's internals. The purpose of this document is to facilitate this ramp up by clearly communication how the project is built to avoid any blockers and idle time during actual development.

This document covers Mirrors 1.1.0 exclusively but it expected to be maintained for each new version of the software.

Mirrors is a totally rad. server side technology for games that provides such awesome features as: Login, Social Connection, Friends, Leaderboards, Cloud Save, Turn Based Matches, and Push Notifications with the intent of serving those features for 100K concurrent users or ~1.5M DAU+.

In this document:

- [Mirrors Repositories](#markdown-header-mirrors-repositories)
- [Mirrors Structure](#markdown-header-mirrors-structure)
    - [Mirrors Admin](#markdown-header-mirrors-admin)
    - [Mirrors Appserver](#markdown-header-mirrors-appserver)
    - [Mirrors Modes](#markdown-header-mirrors-modes)
- [Mirrors Deployment](#markdown-header-mirrors-deployment)
- [Mirrors Data](#markdown-header-mirrors-data)
    - [PostgreSQL](#markdown-header-postgresql)
    - [S3](#markdown-header-s3)
- [Mirrors Code](#markdown-header-mirrors-code)
    - [Nodejs](#markdown-header-nodejs)
    - [Branches](#markdown-header-branches)
    - [Root](#markdown-header-root)
    - [Lib](#markdown-header-lib)
    - [Express](#markdown-header-express)
    - [Swagger](#markdown-header-swagger)
    - [Convict](#markdown-header-convict)
    - [Data Access Manager](#markdown-header-data-access-manager)
    - [Freezer](#markdown-header-freezer)
    - [Node Modules](#markdown-header-node-modules)
    - [Debugging](#markdown-header-debugging)
- [Useful Links](#markdown-header-useful-links)
- [Local Environment](#markdown-header-local-environment)
    - [Installation](#markdown-header-installation)
    - [S3 and Riak-CS](#markdown-header-s3-and-riak-cs)
    - [SQS and ElasticMQ](#markdown-header-sqs-and-elasticmq)
    - [Configuration](#markdown-header-configuration)


- - - -

Mirrors Repositories
====
The Mirrors server code is subdivided into multiple repositories, some of which contain mirrors code while others contain forked software with custom modifications. All repos are available on bitbucket.org and require IT to provide access to the repositories.

1. **[https://bitbucket.org/bhvr_mlb/konduto-api](https://bitbucket.org/bhvr_mlb/konduto-api)** The principal repository where the Mirrors code is stored. It contains multiple branches of which 1.1.0-dev and 1.1.0-release are the principals (not master/release). These branches have the following merge pattern: 1.1.0-dev => 1.1.0-release => {project}-1.1.0-dev => {project}-1.1.0-release
2. **[https://bitbucket.org/bhvr_mlb/mirrors-management](https://bitbucket.org/bhvr_mlb/mirrors-management)** This is a particular repository that only contains controllers used by the admin server. The code was separated as a matter of security and it is included as a module during the npm install process.
3. **[https://bitbucket.org/bhvr_mlb/konduto-admin](https://bitbucket.org/bhvr_mlb/konduto-admin)** The admin interface is hosted here. The admin interface can point to any project-environment server so it needs only to be deployed in 1 place.
4. **[https://bitbucket.org/bhvr_mlb/mirrors-tsung](https://bitbucket.org/bhvr_mlb/mirrors-tsung)** Tsung is the technology used to load test mirrors and this repository keeps all the test files and scripts used to deploy those tests.
5. **[https://bitbucket.org/bhvr_mlb/swagger-node-express](https://bitbucket.org/bhvr_mlb/swagger-node-express)** A fork of the swagger-node-express project with a custom modification that allows layering functions before controller actions are executed. Swagger 2.0 might deprecate the original repo this is forked from while simultaneously supporting the required feature we use.
6. **[https://bitbucket.org/bhvr_mlb/publishing-pushd](https://bitbucket.org/bhvr_mlb/publishing-pushd)** A fork of the pushd project with a custom modification that allows a subscriber to be it's own ID. This modification goes against the intent of the project maintainer and will never be merged. We should work actively to stop using this branch.

- - - -

Mirrors Structure
====
The Mirrors 1.1.0 represents a partial migration to a key-value storage from a [PostgreSQL](http://www.postgresql.org/) storage. Its deployment is complex as it includes many modes that switch the Mirrors code from an API to other jobs.

Mirrors Admin
----
This instance of Mirrors is installed when simply running “npm install”. It installs all the required Mirrors modules as well as the administration controllers module (mirrors-management). In a production environment it is deployed to a single server only accessible from within the Bhvr IP space, typically restricted to the physical building of the office, which requires everyone to have VPN access to the building should something happen to on a production environment that would require a fix through the admin controllers from an employee at home.

Mirrors Appserver
----
This instance of Mirrors is identical to the Admin one save for the fact that the admin controllers module (mirrors-management) is not installed. This happens when executing “npm install –production” and is used specifically for appservers that are deployed on public ip spaces.

Mirrors Modes
----

#### api
This is the default mode that Mirrors is deployed in. It provides access to the api in typical web server behaviour. **Loads lib/main.js**

#### consumer
This mode is meant to asynchronously generate social leaderboard results for players by consuming messages from an SQS queue made for this purpose. It will poll the SQS queue at a configured rate for a configured wait time and process messages there in. A scaling strategy is being devised to be better scale the consumer servers based on queue demand. **Loads lib/consumer.js**

#### global-consumer
This mode is meant to asynchronously generate a public leaderboards by leveraging redis to automatically sort leaderboard results and then periodically store results to a persistent storage. It also consumes messages from an SQS queue dedicated to these types of messages. **Loads lib/consumers/globalconsumer.js**

- - - -

Mirrors Deployment
====
This is a graphical representation of what the current Mirrors deployment looks like for 1.1.0

> ![Mirrors Deployment schematic](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/Mirrors-Deployment.png?at=docs)

This graphic was generated from [http://draw.io](http://draw.io). The XML representation of this file is located in the docs folder for future edits.

- - - -

Mirrors Data
====
Mirrors 1.1.0's migration away from PostgreSQL to S3 has caused only some of its data to move away from the Relational Database into a Key-Value store. 

PostgreSQL
----
On the PostgreSQL side of things, Users, Admins, Achievements, Scheduled Events, Revisions, and Turn Based Matches are still managed through relational tables. A PGModeler diagram is provided here.

> ![Mirrors PostgreSQL PGModeler diagram](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/postgres_relations.png?at=docs)

This graphic was geenrated from [PGModeler](http://www.pgmodeler.com.br/) and it's correspondig .DBM file is located in the docs folder for future edits.

S3
----
For S3, we find Friends, Leaderboards, and States organized under a player id folder which is categorized in a folder divided hash. 

#### Player Keys
The hash is calculated from the player ID and the first 2 and following 3 characters are used to subgroup the number of users into manageable chunks for client machines to operate against S3. The Friends document contains an array of friend IDs, while the playerLeaderboards document contains an object with the leader board IDs follow by the player's own entries. The socialLeaderboards document, on the other hand, contains an object with the leader board IDs and each friend's entry therein. It is updated from a Consumer mode Mirrors server. 

> ![Mirrors S3 player storage](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/S3_player.png?at=docs)

#### Player States
The state folder is used to store all states that the client wants saved. It contains, by default, an all_keys document listing the documents in the state folder that is kept up to date by the Mirrors code. Each setting is then plainly listed as a key to be fetched for clients (in this example, 2 states are provided for illustration: PlayerWallet, and PlayerSettings).

> ![Mirrors S3 player states storage](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/S3_player_state.png?at=docs)

#### Mirrors keys
Mirrors itself, as a service, stores general data for the games in its own keys. Instead of a player ID it uses “Mirrors”, so its resulting computed hash end up being: 80b3ba96302faf888bb5e25d38648f00 which will be resolved to go to bucket 10. Currently it only stores the list of leaderboards defined by the game, but could be used to store other, equally generic data that applies to all users and to the application as a whole.

> ![Mirrors S3 mirrors general storage](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/S3_Mirrors.png?at=docs)

- - - -

Mirrors Code
====
Mirrors' code is written in NodeJS and is divided between some ancillary files in the root and the core logic in the lib folder.

Nodejs
----
[Nodejs](http://nodejs.org/) is a Javascript platform built on top of [Google V8 engine](https://developers.google.com/v8/) for server technology. Nodejs uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices. In practice this means that Nodejs is axed on distributed communication thanks to its single-process-per-core application design and asynchronous execution thanks to javascript callbacks.

Branches
----
The current active branches of the project are “1.1.0-dev” and “1.1.0-release”. If a new feature is in development, it is expected to be developed in a separate branch and that a pull request is made to review and approve the merge to 1.1.0-dev. In addition to these basic branches, each project gets their own branches that use the same name with their project as a prefix, for example: MyProject-1.1.0-dev and MyProject-1.1.0-release. The planned merge route for all these branches is _FeatureBranch_ → _1.1.0-dev_→  _1.1.0-release_ → _{ProjectName}-1.1.0-dev_ → _{ProjectName}-1.1.0-release_.

Root
----
The principal, and only, entry point is the app.js file located at the root of the project. The project root also includes:

> ![Mirrors root](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/mirrors_root.png?at=docs)

- **app.js** The entry point to Mirrors code. Run “node api.js” to start a server. This file also contains the code that switches between modes.
- **bin** Contains some scripts for setting up various environments, including aws appservers and local environments.
- **config** Where all configs are defined. The index.js file contained within uses the [Convict](https://github.com/mozilla/node-convict) framework to describe all the possible configuration options and then loads all the .json files contained in the folder in alphabetical order to give a consistent over writting pattern to the files. **It is recommended for developers to use a settings.local.json** which will automatically be ignored by git to set local settings.
- **docs** Contains some documentation regarding the framework.
- **Gruntfile.js** Is the file that describes some complex jobs that can be accomplished using [Grunt](http://gruntjs.com/). A look inside will reveal the different jobs available, more can be added as well to help with development and deployment.
- **lib** Principle folder where actual server logic is held. It has its own section bellow.
- **node_modules** Where external libraries used by Mirrors are stored through the “npm install” command.
- **package.json** The file that lists all the external libraries used by Mirrors. The “npm install” parses this file and installs the required module.
- **projects** Holds some SQL files to insert some data into a fresh database. Usually includes a game and 1 admin user with the name: dsa and the password: dsa. 
- **sqitch** Where [Sqitch](http://sqitch.org/) schema files are stored and deployed from.
- **test** All test files are stored here and are executed using the [Mocha](http://mochajs.org/) framework .

Lib
----
Where all the actual logic of the server is stored.

> ![Mirrors lib](https://bitbucket.org/bhvr_mlb/konduto-api/raw/1.1.0-dev/docs/mirrors_root_lib.png?at=docs)

- **consumer.js** The file that is executed when the mode is set to “consumer” and fills in social leader boards for players.
- **consumers** Contains the files used by the global-consumer which are executed when the mode is set to “global-consumer” to fill the global leader boards.
- **controllers** This folder **contains the controllers that defines the API** that is exposed to the clients. All controllers use the swagger protocol to define the API endpoint, document its use, and declare the function executed by the controller simultaneously. Each file within segments a section of the api.
- **dataAccessManager** This folder contains the Data Access Layers and the Data Access Manager that uses them. A generic Data Access Layer is also provided as a template to inherit from.
- **db.js** The file that takes care of connecting to the PostgreSQL database.
- **freezer** A custom built ORM for Mirrors used by the controllers and models that are still tied to the PostgreSQL database.
- **httpUtils** A helper file used by the controllers to consistently return responses and log errors.
- **logObject** A helper file used to construct consistent errors that we can easily trace back to file/lines.
- **main.js** The principal file loaded by the app.js file when in the default api mode. This file takes care of loading the controllers and setting up extra express functions required.
- **modelHelper.js** A helper file used by the models.
- **models** The folder where all object models are defined. They are setup using the provided index.js file.
- **push.js** The file used by the turn based match model to talk to the PushD. PushD is a separate service we deploy on a server within our AWS environment.
- **swagger_models.js** The file that defined the swagger request object as they would be listed in the documentation of the controllers.

Express
----
[Express](http://expressjs.com/) is a web framework for nodejs that is used as a core for Mirrors. It provides routing, middleware and other useful features. It's design is light on features, capitalizing instead on extensibility. When manipulating [request](http://expressjs.com/3x/api.html#req.params) or [response](http://expressjs.com/3x/api.html#res.status) objects (name req and res respectively in functions), the express documentation on those objects is the reference to follow.

Swagger
----
[Swagger](http://swagger.io/) is an API framework. On its own, Swagger is just a protocol. It simply provides a standard into which an API is described. The actual library used to implement the swagger protocol is the [swagger-node-express](https://github.com/swagger-api/swagger-node-express) module. In concrete terms, it allows us to describe an API in such a way as the description can serve as the documentation, the execution and the testing interface. 

Convict
----
[Convict](https://github.com/mozilla/node-convict) is a Mozilla project aimed at expanding on the standard pattern of configuring node.js applications in a way that is more robust and accessible to collaborators, who may have less interest in digging through imperative code in order to inspect or modify settings. By introducing a configuration schema, convict gives project collaborators more context on each setting and enables validation and early failures for when configuration goes wrong. It is used in the /config/index.js where in all json files are read.

Data Access Manager
----
The Data Access Manager, or DAM, is used to easily and transparently access key-value type data without having to deal with the specific implementation of the storage method used. It is setup by providing storage layers in a specific order that range from short lived, memory type, caches up to a long lived, disk written, types of storage. Currently, 2 layers are provided, a [Redis](http://redis.io/) layer used as a cache, and an [S3](http://aws.amazon.com/s3/) layer for long term storage. More layers could be added in the future to increase performance, such a session buffer layer, to store values in memory. A GET request using the DAM will query the layers in the order they are set up, and on cache misses, will escalate to the next layer, until a value is found, or return an error if the value does not exist in any layer. A SET request using the DAM would set the value for all layers asynchronously. 

Freezer
----
Freezer is the custom built ORM layer used by Mirrors when communication with the PostgreSQL database. It is currently in progressive deprecation with the goal of having it completely removed by Mirrors 1.2.0 and as such its documentation is relegated to a separate document.

Amazon Web Services
----
[AWS](http://aws.amazon.com/) is the platform on which we deploy our server solutions. As such it is not wasted time to get acquainted with their products, documents, services, and price scheme. We currently use many of their services such as [EC2](http://aws.amazon.com/ec2/), [RDS](http://aws.amazon.com/rds/), [Elasticache](http://aws.amazon.com/elasticache/) and [S3](http://aws.amazon.com/s3/). AWS has other products that they offer which also change over time. It is a good idea to keep an eye out on what's happening to see if any of that can be leveraged to ease both the programmer's and IT's job.

Node Modules
----
The nature of a Nodejs project involves using many modules to accomplish different task. For this reason, a list is compiled here of some of the more common modules used in the code base to better be able to concentrate on what to get familiar with. The list was generated using this command:

    find ./ -exec grep --color -sHn require\( '{}' \; | grep -o require\(.*\) | sort | uniq -c | sort -hr

- **[Swagger-node-express](https://github.com/swagger-api/swagger-node-express)** This module was mentioned earlier but it is worth mentioning again. It is the module used to simultaneously define, document, and test the API controllers provided by Mirrors. All the controllers are written respecting this module's controller object format.
- **[Async](https://github.com/caolan/async)** The async module is a very useful when trying to manage the completion of multiple asynchronous functions.
- **[Lodash](https://lodash.com/docs)** Similar to the [underscorejs](http://underscorejs.org/) project, this is a helper module that provides extra functions for working with arrays and objects.
- **[Winston](https://github.com/flatiron/winston)** A generic logging module which is used to generate [Logstash](http://logstash.net/) compatible logs for collection.
- **[String](http://stringjs.com/)** A helper module that provides extra functionality for dealing with strings.
- **[Aws-sdk](http://aws.amazon.com/sdk-for-node-js/)** A very important module that allows us to use AWS products such as S3 and SQS through our code.
- **[Q](http://documentup.com/kriskowal/q/)** Not actually used very often but it is worth mentioning. This module provides the programmer an alternative to deeply nested callbacks called Promises. The resulting code when using Q's Promises can be confusing to read and as a consequence it is recommended to only use it if the alternative is a worse looking callback “pyramid” or to avoid stack busting within a recursive function.

Debugging
----
Nodejs provides a very accessible debugger using a [Google Chrome](http://www.google.com/chrome/) or [Chromium](http://www.chromium.org/) browser (since both use the V8 engine for JS), simply execute:

    $ npm install -g node-inspector
    $ node-debug app.js
    debugger listening on port 5858 
    Node Inspector is now available from http://localhost:8080/debug?port=5858
And pointing your browser to the address will connect you to the debugger.

- - - -

Useful Links
====
These links are useful to have on hand when developing on Mirrors projects.

Mirrors
----
- **http://sbunix-publish-01/#/login** The Admin panel where all games are managed. It is a single standalone interface that uses the Admin controllers to manage data for the games.
- **http://869614383496.signin.aws.amazon.com/console** The AWS web console used to manage the Backend project. The Backend project is meant for internal testing of Mirrors and includes Dev and Stress environments. The stress environment for stress testing the Mirrors technology. This console includes read-only credentials, user: ro, password: VYVjXGLJ2DISnHld but IT should give personal creds with sufficient read/write access to all server programmers.
- **http://sbunix-backend-01:8080/** The Jenkins deployment machine that takes care of deploying new servers to the Backend project's Dev and Stress environments.
- **https://stage-status.bhvronline.com/** The [Stashboard](http://www.stashboard.org/) status page used for all projects' Dev, QA, UAT, and Stage environments. Stashboard itself is used an api availability monitor to indicate if the Mirrors api is up, down, or under maintenance. It is managed entirely by IT and used exclusively by client programmers. Stashboard does not involve server programmers at all.
- **http://jsoneditoronline.org** Just a useful website for viewing JSON objects.

Home
----
- **http://sbw8-home-01.a2m.com/** The Jenkins machine used to build the Home client applications for iOS and Android.
- **http://sbunix-home-01:8080** The Jenkins deployment machine used to deploy Mirrors onto Home's Dev, QA, UAT, Stage, and Live environments.
- **https://992458580694.signin.aws.amazon.com/console** The AWS web console for Home's dev/qa/uat/stage servers. It has read-only credentials, user: ro, password: C8Q2UD2oAQgJiBia

Golf
----
- **http://sbunix-rgolf-01:8080/** The Jenkins machine to both, build client applications for iOS and Android as well as deploy the Mirrors code to Dev, QA, UAT, Stage and Live environments for the Real Feel Golf (formerly Pro Feel Golf) project.
- **http://svrlx-poc-01.a2m.com/** A special resource created by IT that communicates the AWS [CloudWatch](http://aws.amazon.com/cloudwatch/) data to a read-only website with graphs.

HTC
----
- **http://sbunix-htc-01:8080/** The Jenkins machine to both, build client applications for iOS and Android as well as deploy the Mirrors code to Dev, QA, UAT, Stage, and Live environments for the Here They Come (HTC) project.
- **https://176471082016.signin.aws.amazon.com/console** The AWS web console address for HTC's Dev, QA, UAT, and Stage environments.

Underground
----
- **http://macmini-sgosvr.chile.a2m.com:8080/** The Jenkins build machine for Santiago's Underground Project.

- - - -

Local Environment
====

Installation
----
These installation steps are specifically for a local environment running Ubuntu 14.04 LTS. Run these commands blindly:

    $ sudo apt-get install nodejs git make build-essential postgresql postgresql-server-dev-9.3 redis-server redis-tools curl
    $ sudo cpan App::Sqitch DBD::Pg
    $ echo "createuser -Pe $USER" | sudo -H -u postgres bash
    $ echo "createdb -O $currentuser $USER" | sudo -H -u postgres bash
    $ npm install -g supervisor yo mocha grunt-cli node-inspector
    $ sudo echo “root soft nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “root hard nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “stanchion soft nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “stanchion hard nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “riakcs soft nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “riakcs hard nofile 65536” >> /etc/security/limits.conf
    $ sudo echo “session required pam_limits.so” >> /etc/pam.d/common-session
    $ git clone git@bitbucket.org:bhvr_mlb/konduto-api.git
    $ cd konduto-api
    $ ./bin/local.s3.sh
    $ ./bin/local.sqs.sh

What these steps do is effectively 

1. Install nodejs, postgres, and redis through the ubuntu package manager.
2. Setup a postgres user and database that can be accessed simply by typing “psql”.
3. Install some nodejs utilities that will be globally accessible (hence the -g flag).
4. Increase the limit of open files for certain users. *This step may require rebooting.*
5. Checkout the main Mirrors project.
6. Install a local instance of Riak-CS to get an S3 compatible storage API (which installs an erlang version manager called kerl, downloads, compiles and installs an appropriate erlang version to then download, compile, and install Riak 1.4 and then install Riak-CS through a new software source). Follow the setup instructions at this step and keep note of the key and secret generated it will generate for a user called “dsa”. This step intalls system wide services accessible by running “sudo riak”, “sudo stanchion”, and “sudo riak-cs”.
7. And Finally download and run a Java based queue service that is API compatible with SQS. Look at the bin/local.sqs.sh file to see how to start this service up. It is very simple.

S3 and Riak-CS
----
[Riak-CS](http://basho.com/riak-cloud-storage/) is used to create a local storage that is API compatible with Amazon's S3. You will then need to create your S3 bucket within your local Riak-CS instance to start using it in Mirrors.

    $ sudo apt-get install s3cmd
    $ s3cmd --configure

Running these commands and following the configuration instructions should leave you with a  ~/.s3cfg file. Just make sure that the following parameters also match up with the data you entered.

    host_base = s3.localhost                                                                            
    host_bucket = %(bucket)s.s3.localhost  

Finally, run:

    $ s3cmd mb s3://localbucket

And you're done. You now have a local bucket you can query using s3cmd! For more information consult the “getting started” guide for [Riak-CS](http://basho.com/riak-cloud-storage/).
Should you need to restart Riak-CS, you will need to run the following commands in order:

    $ sudo riak-cs stop
    $ sudo stanchion stop
    $ sudo riak stop
    
    $ sudo riak start
    $ sudo stanchion start
    $ sudo riak-cs start

SQS and ElasticMQ
----
[ElasticMQ](https://github.com/adamw/elasticmq) is a small Java application that offers a queue service that is API compatible with Amazon's. It can be simply executed by running this command:

    $ java -Dconfig.file=bin/localsqs.conf -jar bin/elasticmq-server-0.8.5.jar

From there you will have to create and execute a small nodejs script to create your queue for you as there is no direct interface to ElasticMQ. You should create this file in the same directory as the app.js file and execute it to create your mirrors-dev-leaderboards queue.

    var AWS = require('aws-sdk'); 
    AWS.config.update({"region":"us-east-1", "accessKeyId":"derp", "secretAccessKey":"herp"}); 
    var sqs = new AWS.SQS({"endpoint":"http://localhost:9910"});
    
    sqs.createQueue({"QueueName":"mirrors-dev-leaderboards"}, function(err, data) { 
        console.log(err, data); 
    }); 
    
    sqs.listQueues({}, function(err, data) { 
        console.log(err, data); 
    }); 

Furthermore, this queue only lives as long as the java process, it is not stored for long term use after the application terminates, so keep these scripts handy.

Configuration
----
You will also need to create a settings.local.json file in the config folder. This file, since it contains the .local.json extension, will be ignored by git so there is no risk of comitting it to the project. Your local settings folder should look like this:

    {
        "mode": "api",
        "db": "postgres://username:pass@localhost:5432/username",
        "poolSize":20,
        "aws_access_key_id":"your previously generated access key",
        "aws_secret_access_key":"your previously generated secret",
        "aws_region":"us-east-1",
        "aws_s3_endpoint":"http://localhost:8080",
        "aws_bucket":"localbucket",
        "aws_shards":"0",
        "aws_s3_forcepathstyle":true,
    
        "aws_sqs_endpoint":"http://localhost:9910",
        "aws_sqs_leaderboard_queue": "http://localhost:9910/queue/mirrors-dev-leaderboards",
        "aws_sqs_leaderboard_max_messages": 10,
        "aws_sqs_leaderboard_visibility_timeout": 120,
        "aws_sqs_leaderboard_wait_time": 5,
        "aws_sqs_leaderboard_interval": 1000,
    
        "maximum_redis_leaderboard_size": 100,
        "redis_pruning_threshold": 120,
        "aws_sqs_global_leaderboard_queue": "http://localhost:9910/queue/mirrors-dev-global-leaderboards",
        "aws_sqs_global_leaderboard_poll_interval": 1000,
        "aws_sqs_global_leaderboard_transfer_interval": 60000
    }

### Important
- The db parameter can look like this if you have no password set:
    "db": "postgres://username@localhost:5432/username", 
- The aws_bucket parameter must match the bucket name you created in Riak-CS previously.
- The aws_sqs_leaderboard_queue parameter must match the queue that you created in your local ElasticMQ.
- Remember that all config parameters are described in detail in the config/index.js using convict notation.
