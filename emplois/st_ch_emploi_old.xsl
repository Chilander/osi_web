<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-public="-//w3c//DTD HTML 4.01 Transitional//EN"></xsl:output>
<xsl:template match="/">
<xsl:for-each select="emplois">
	<html>
	<head>
		<title>Poste - <xsl:value-of select="titre"/></title>
			<style type="text/css">
				.content3
				{
					line-height:14px;
				}
				.style1 {
					font-family: Arial, Helvetica, sans-serif;
					font-weight: bold;
					color: #0C2D83;
				}
				.style2 {color: #0C2D83}
				.style3 {
					font-family: Arial, Helvetica, sans-serif;
					color: #FFFFFF;
					font-size: 12px;
					font-weight: bold;
				}
				.style4 {
					font-family: Arial, Helvetica, sans-serif;
					font-size: 12px;
					color: #0C2D83;
					}
				html,body 
				{
					margin-left: 0px;
					margin-top: 0px;
					margin-right: 0px;
					margin-bottom: 0px;
					height: 100%;
				}
				.th
				{
					height: 100%;
				}
		</style>
	</head>

	<body>
	  <table class="th" height="100%" width="600" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td valign="top">
				<table class="th" height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
<!--Rang 1 -->
					<tr>
					  <td width="176" height="66" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20">&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 2 -->
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" height="76"><img src="../images/osi_logo.gif" width="175" height="76"></img></td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 3 -->					
					<tr>
					  <td width="176" height="65" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 4 -->					
					<tr>
					  <td width="176" height="19" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td><span class="style2"></span></td>
					  <td width="450"><span class="style1"><xsl:value-of select="titre"/></span><br></br>
					  <span class="style4"><xsl:value-of select="code"/></span>
					  </td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 5 -->
					<tr>
					  <td width="176" height="19" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 6 -->
					<tr valign="top">
					  <td width="176" height="21" align="right" background="../images/bg-osi.gif"><div align="right">
						<p class="style3"><xsl:value-of select="date"/><br></br>
						  Poste <xsl:value-of select="type"/><br></br>
							<xsl:if test="type = 'Contractuel'">
								<xsl:value-of select="duree"/><br></br>
						   </xsl:if>						  
						  <xsl:value-of select="localisation"/>
						</p>
						</div></td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">
							<table class="th" height="100%" width="450" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<span class="style4">
											<xsl:for-each select="description">
											   <xsl:value-of select="text"/><br></br>
											</xsl:for-each>
										</span><br></br>
									</td>
								</tr>
								<tr>
									<td>
										<span class="style4">
											<ul>
												<xsl:for-each select="expertise">
												   <li><xsl:value-of select="text"/></li>
												</xsl:for-each>
											  </ul>
										</span>
									</td>
								</tr>
	        					<xsl:if test="technologie != ''">
								<tr>
									<td><span  class="style4">
										<b>Exigences technologiques : </b><br></br>
										<blockquote>
											<xsl:for-each select="technologie">
											   <xsl:value-of select="text"/><br></br>
											</xsl:for-each>										  
										</blockquote>
									</span></td>
								</tr>
 						       </xsl:if>
							</table>
					  </td>
					  <td width="20">&#160;</td>
					</tr>
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" class="style4">
						  <xsl:element name="a">
							<xsl:attribute name="href">mailto:cv@gcosi.com?subject=Emploi <xsl:value-of select="code"/> - <xsl:value-of select="titre"/>
							</xsl:attribute>
							<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066">Cliquez ici pour postuler.</font>
						</xsl:element>
						&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
						  <xsl:element name="a">
							<xsl:attribute name="href">javascript:window.print()</xsl:attribute>
							<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066">Cliquez ici pour imprimer la page.</font>
						</xsl:element>
					  </td>
					  <td width="20">&#160;</td>
					</tr>
					<tr class="th">
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
				</table>
			</td>
		</tr>
	  </table>

	</body>
	</html>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>