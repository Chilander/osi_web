<?php
	$qType = $_GET['type'];
	if($qType == 'Permanent')
	{
		$aType = 'Permanant';
	}
	else
	{
		$aType = 'Contractual';
		$qType = 'Contractuel';
	}
	$qProfil = $_GET['profil'];
	switch($qProfil)
	{
		case "1":
		$aProfil = "Management";
		break;
		case "2":
		$aProfil = "Development";
		break;
		case "3":
		$aProfil = "Infrastructure";
		break;
		case "4":
		$aProfil = "Telecom / Engineering";
		break;
		case "5":
		$aProfil = "Enterprise resource planning (ERP)";
		break;
		case "6":
		$aProfil = "Other expertise";
		break;
	}

    $xml = new DomDocument('1.0'); 
    $xml->load('emplois.xml'); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("lib/inc/header.html");?>
        <style type="text/css">
            .thiscontent3
            {
                line-height:14px;
            }
			.thiscontent4
            {
                line-height:10px;
            }
            html,body 
            {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
                height:100%;
            }
            table.th
            {
                height: 100%;
            }
       		.style2 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
			}
       		.style3 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}			
			.style4 {color: #CCCCCC}
			
			a
			{
			color:gray;
			text-decoration:none;
			}
			a:visited {color:black}
			a:active  {color:mediumblue}
			a:hover   
			{
			color:mediumblue;
			text-decoration:underline;
			}
        </style>
    </head>	
    
    <body bottommargin="0" topmargin="0" marginheight="0" marginwidth="0">
            <table class="th" height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
 				<?php include("lib/inc/flashnav.html");?>
                
                <tr class="th">
                    <td background="../images/bg.gif">&#160;</td>
                    <td background="../images/bordure_gauche.jpg" width="10">&#160;</td>
                    <td width="750" valign="top">
                        <table height="100%" width="749" border="0" cellpadding="10" cellspacing="0">
    
    
                            <tr valign="top">
                              <td align="center">
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>Here are our current openings.</b></font>
                                    <br />
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><?php echo "$aType - ".(($qProfil == '') ? 'All profiles' : $aProfil)?></font><br>
                                    </br>
                                    
                                    <?php
										$emplois = $xml->getElementsByTagName("item");
										
										foreach($emplois as $itemEmploiNode)
										{
											//$titre = $itemEmploiNode->getElementsByTagName('titre')->item(0);
											$titres = $itemEmploiNode->getElementsByTagName("titre");
											$titre = $titres->item(0)->nodeValue;
											$titre = iconv("UTF-8", "ISO-8859-1", $titre);
											
											$nouveaux = $itemEmploiNode->getElementsByTagName("nouveau");
											$nouveau = $nouveaux->item(0)->nodeValue;
									
											$categories = $itemEmploiNode->getElementsByTagName("category");
											$category = $categories->item(0)->nodeValue;
											
											$dates = $itemEmploiNode->getElementsByTagName("date");
											$date = $dates->item(0)->nodeValue;
											$date = iconv("UTF-8", "ISO-8859-1", $date);
									
											$localisations = $itemEmploiNode->getElementsByTagName("localisation");
											$localisation = $localisations->item(0)->nodeValue;
											$localisation = iconv("UTF-8", "ISO-8859-1", $localisation);
									
											$durees = $itemEmploiNode->getElementsByTagName("duree");
											$duree = $durees->item(0)->nodeValue;
											$duree = iconv("UTF-8", "ISO-8859-1", $duree);
									
											$codes = $itemEmploiNode->getElementsByTagName("code");
											$code = $codes->item(0)->nodeValue;
											$code = iconv("UTF-8", "ISO-8859-1", $code);
									
											$descriptions = $itemEmploiNode->getElementsByTagName("description");
											$texts = $descriptions->item(0)->getElementsByTagName("text");
											$description = $texts->item(0)->nodeValue;
											$description = iconv("UTF-8", "ISO-8859-1", $description);
											
											$types = $itemEmploiNode->getElementsByTagName("type");
											$type = $types->item(0)->nodeValue;
											
											$profils = $itemEmploiNode->getElementsByTagName("profil");
											$profil = $profils->item(0)->nodeValue;
									
											//echo "$titre"." "."$nouveau"." "."$category"." "."$date"." "."$localisation"." "."$duree"." "."$code"." "."$description"." "."$type<br>";
									if(($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
                                    echo '
                                    <table cellspacing="0" cellpadding="5" bordercolor="#CCCCCC" border="0">
                                            <tr><td>
                                                <table width="400" border="1" cellpadding="0" cellspacing="0" bordercolor="#000066">
                                                <tr><td>
                                                <table width="398" border="0" cellspacing="0" cellpadding="0">
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr valign="bottom">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
    
                                                        <td width="200" bgcolor="#FFFFFF" align="left">
														';
														if($nouveau == "true" && ($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
                                                        	echo '<font face="Arial, Helvetica, sans-serif" size="-2" color="#FF0000"><b>NEW OPENING</b></font><br>';
														if(($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
														echo
														'
														<a href=javascript:OpenWindow("'.$code.'")>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>'.$titre.'</b></font></a><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$date.'</font><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$type.'</font>
                                                        </td>
														
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="125" bgcolor="#FFFFFF" align="left">
														<a href=javascript:OpenWindow("'.$code.'")>
															<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066" class="thiscontent4">Click here for details</font>
                                                        </a>
														</td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>																									
                                                </table>
                                                </td></tr>
                                                </table>
                                            </td></tr>
                                    </table>';
										}
                                    ?>
                                    <br />
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">Please note that the company has a program of equal access to employment and we invite all targeted group members, women, visible minorities, handicapped persons and aboriginal peoples  to apply for this position. The use of the masculine gender is only intended to allege the text.</font>
                                    <br />
                              </td>
                                <td valign="top" align="center" width="190" padding="20" background="../images/bg-1.gif"><div align="left">
                                <p class="style2">Please choose a category corresponding to your profile:</p>
                                <p class="style3">
                                  <a href="jobs.php?type=<?php echo $qType; ?>">All profiles</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=1">Management</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=2">Development</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=3">Infrastructure</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=4">Telecom / Engineering</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=5">Enterprise resource planning (ERP)</a><br />
                                  <a href="jobs.php?type=<?php echo $qType; ?>&profil=6">Other expertise</a><br />
                                </p>
                                  <br />
                                  <br />
                                  <img src="../images/carriere.jpg" width="185" height="307" border="1"></img>
                                </div></td>
                            </tr>
                        </table>						
                    </td>
                    <td background="../images/bordure_droite.jpg" width="10">&#160;</td>
                    <td background="../images/bg.gif">&#160;</td>
                </tr>
                <?php include("lib/inc/footer.html");?>
            </table>
    </body>
</html>
