<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<?php
	
    $xml = new DomDocument('1.0'); 
    $xml->load('emplois.xml'); 
	
	$emplois = $xml->getElementsByTagName("item");
	
	foreach($emplois as $itemEmploiNode)
	{
		//$titre = $itemEmploiNode->getElementsByTagName('titre')->item(0);
		$titres = $itemEmploiNode->getElementsByTagName("titre");
		$titre = $titres->item(0)->nodeValue;
		
		$nouveaux = $itemEmploiNode->getElementsByTagName("nouveau");
		$nouveau = $nouveaux->item(0)->nodeValue;

		$categories = $itemEmploiNode->getElementsByTagName("category");
		$category = $categories->item(0)->nodeValue;
		
		$dates = $itemEmploiNode->getElementsByTagName("date");
		$date = $dates->item(0)->nodeValue;

		$localisations = $itemEmploiNode->getElementsByTagName("localisation");
		$localisation = $localisations->item(0)->nodeValue;

		$durees = $itemEmploiNode->getElementsByTagName("duree");
		$duree = $durees->item(0)->nodeValue;

		$codes = $itemEmploiNode->getElementsByTagName("code");
		$code = $codes->item(0)->nodeValue;

		$descriptions = $itemEmploiNode->getElementsByTagName("description");
		$texts = $descriptions->item(0)->getElementsByTagName("text");
		$description = $texts->item(0)->nodeValue;
		
		$types = $itemEmploiNode->getElementsByTagName("type");
		$type = $types->item(0)->nodeValue;

		//echo "$titre"." "."$nouveau"." "."$category"." "."$date"." "."$localisation"." "."$duree"." "."$code"." "."$description"." "."$type<br>";
	}
?>

<body>
</body>
</html>
