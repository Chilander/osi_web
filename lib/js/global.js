var match = new Array
(
 	{fr:"accueil", en:"home"},
	{fr:"bienvenue", en:"welcome"},
	{fr:"carriere_chez_osi", en:"career_at_osi"},
	{fr:"contact", en:"contact"},
	{fr:"developpement_durable", en:"sustainable_development"},
	{fr:"emplois", en:"jobs"},
	{fr:"entreprise", en:"entreprise"},
	{fr:"nos_consultants", en:"our_consultants"},
	{fr:"notre_approche", en:"our_approch"},
	{fr:"placement_permanent", en:"permanent_placement"},
	{fr:"services_de_consultation", en:"consulting_services"},
	{fr:"services_de_detail", en:"retail_services"},
	{fr:"nouvelles", en:"news"},
	{fr:"developpement_applicatif", en:"application_development"},
	{fr:"services_geres", en:"managed_services"}
);

function enregistrerCookie(valeur, expireDate) {
	document.cookie = "lang=" + escape(valeur) + "; expires=" + expireDate + "; path=/";
}
function makeCookieEn() {
	result = "en";
	var unJour = 24 * 60 * 60 * 1000; //variable en milliseconde d'un jour
	var expDate = new Date(); //variable objet de date
	expDate.setTime (expDate.getTime() + (999*unJour)); //calculer la date d'expiration
	enregistrerCookie(result, expDate.toGMTString()); //appel de la fonction
}
function makeCookieFr() {
	result = "fr";
	var unJour = 24 * 60 * 60 * 1000; //variable en milliseconde d'un jour
	var expDate = new Date(); //variable objet de date
	expDate.setTime (expDate.getTime() + (999*unJour)); //calculer la date d'expiration
	enregistrerCookie(result, expDate.toGMTString()); //appel de la fonction
}
function readCookie() {
	return document.cookie;
}

function getCorrespondingPageName(page)
{
	for(var i = 0; i < match.length; i++)
	{
		if(match[i].en == page)
		{
			return match[i].fr;
		}
		if(match[i].fr == page)
		{
			return match[i].en;
		}
	}
	return "";
}
function english() {
	var thisURL = document.URL;
	var newURL = thisURL.replace("_fr", "_en");
	var page1 = (newURL.split("/")[newURL.split("/").length - 1]).split(".")[0];
	var page2 = getCorrespondingPageName(page1);
	newURL = newURL.replace(page1, page2);
	makeCookieEn();
	location.href=newURL;
}
function francais() {
	var thisURL = document.URL;
	var newURL = thisURL.replace("_en", "_fr");
	var page1 = (newURL.split("/")[newURL.split("/").length - 1]).split(".")[0];
	var page2 = getCorrespondingPageName(page1);
	newURL = newURL.replace(page1, page2);
	makeCookieFr();
	location.href=newURL;
}
function getCurrentUrlLanguage()
{
	var url = document.URL;
	if(url.indexOf("_en") != -1)
	{
		return "_en";
	}
	else
	{
		return "_fr";
	}
}

function openNouvelles(dateNouvelle){
	fichier = "nouvelles/" + dateNouvelle + ".htm";
	myWindow = window.open(fichier, "NouvellesGroupeConseilOSI", "width=760,height=700,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=yes,status=no,left=2,top=100");
}
function OpenWindow(job){
	fichier ="../emplois/" + job + ".xml";
	myWindow = window.open(fichier, "EmploiGroupeConseilOSI", "width=650,height=700,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=yes,status=no,location=no,left=2,top=100");
	myWindow.focus();
}
function openImage(img) {
	myWindow = window.open("../photo/" + img, "OSI", "width=680,height=480,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=no,status=no,location=no,left=2,top=100");
}
function openImageVer(img) {
	myWindow = window.open("../photo/" + img, "OSI", "width=480,height=640,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=no,status=no,location=no,left=2,top=100");
}