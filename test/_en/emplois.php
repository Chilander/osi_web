<?php
	$qType = $_GET['type'];
	if($qType == 'Permanent')
	{
		$aType = 'Permanant';
	}
	else
	{
		$aType = $qType;
	}
	$qProfil = $_GET['profil'];
	switch($qProfil)
	{
		case "Gestion":
		$aProfil = "Management";
		break;
		case "Développement":
		$aProfil = "Developpement";
		break;
		case "Technique":
		$aProfil = "Technical";
		break;
		case "Divers":
		$aProfil = "Others";
		break;
	}

    $xml = new DomDocument('1.0'); 
    $xml->load('emplois.xml'); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Groupe conseil OSI - profils recherchés</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></meta>

        <script type="text/javascript" src="../script.js"></script>
        <script type="text/javascript" src="../Scripts/AC_RunActiveContent.js" ></script>
        <style type="text/css">
            .thiscontent3
            {
                line-height:14px;
            }
			.thiscontent4
            {
                line-height:10px;
            }
            html,body 
            {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
                height:100%;
            }
            table.th
            {
                height: 100%;
            }
       		.style2 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
			}
       		.style3 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}			
			.style4 {color: #CCCCCC}
			
			a
			{
			color:gray;
			text-decoration:none;
			}
			a:visited {color:black}
			a:active  {color:mediumblue}
			a:hover   
			{
			color:mediumblue;
			text-decoration:underline;
			}
        </style>
    </head>	
    
    <body bottommargin="0" topmargin="0" marginheight="0" marginwidth="0">
            <table class="th" height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr height="235">
                    <td background="../images/bg.jpg">&#160;</td>
                    <td background="../images/bordure_gauche_haut.jpg" width="10">&#160;</td>
                    <td width="750">
                    <script type="text/javascript">
                        AC_FL_RunContent( 
                            'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0',
                            'width','750',
                            'height','235',
                            'src','flash1',
                            'quality','high',
                            'pluginspage','http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash',
                            'movie','flash1' ); //end AC code
                    </script>
                    </td>
                    <td background="../images/bordure_droite_haut.jpg" width="10">&#160;</td>
                    <td background="../images/bg.jpg">&#160;</td>
                </tr>
                
                <tr class="th">
                    <td background="../images/bg.gif">&#160;</td>
                    <td background="../images/bordure_gauche.jpg" width="10">&#160;</td>
                    <td width="750" valign="top">
                        <table height="100%" width="749" border="0" cellpadding="10" cellspacing="0">
    
    
                            <tr valign="top">
                              <td align="center">
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>Here are our current openings.</b></font>
                                    <br />
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><?php echo "$aType - ".(($qProfil == '') ? 'All profiles' : $aProfil)?></font><br>
                                    </br>
                                    
                                    <?php
										$emplois = $xml->getElementsByTagName("item");
										
										foreach($emplois as $itemEmploiNode)
										{
											//$titre = $itemEmploiNode->getElementsByTagName('titre')->item(0);
											$titres = $itemEmploiNode->getElementsByTagName("titre");
											$titre = $titres->item(0)->nodeValue;
											
											$nouveaux = $itemEmploiNode->getElementsByTagName("nouveau");
											$nouveau = $nouveaux->item(0)->nodeValue;
									
											$categories = $itemEmploiNode->getElementsByTagName("category");
											$category = $categories->item(0)->nodeValue;
											
											$dates = $itemEmploiNode->getElementsByTagName("date");
											$date = $dates->item(0)->nodeValue;
									
											$localisations = $itemEmploiNode->getElementsByTagName("localisation");
											$localisation = $localisations->item(0)->nodeValue;
									
											$durees = $itemEmploiNode->getElementsByTagName("duree");
											$duree = $durees->item(0)->nodeValue;
									
											$codes = $itemEmploiNode->getElementsByTagName("code");
											$code = $codes->item(0)->nodeValue;
									
											$descriptions = $itemEmploiNode->getElementsByTagName("description");
											$texts = $descriptions->item(0)->getElementsByTagName("text");
											$description = $texts->item(0)->nodeValue;
											
											$types = $itemEmploiNode->getElementsByTagName("type");
											$type = $types->item(0)->nodeValue;
											
											$profils = $itemEmploiNode->getElementsByTagName("profil");
											$profil = $profils->item(0)->nodeValue;
									
											//echo "$titre"." "."$nouveau"." "."$category"." "."$date"." "."$localisation"." "."$duree"." "."$code"." "."$description"." "."$type<br>";
									if(($type == $qType || $type == "") && ($profil == $qProfil || $profil == "" || $qProfil == ""))
                                    echo '
                                    <table cellspacing="0" cellpadding="5" bordercolor="#CCCCCC" border="0">
                                            <tr><td>
                                                <table width="400" border="1" cellpadding="0" cellspacing="0" bordercolor="#000066">
                                                <tr><td>
                                                <table width="398" border="0" cellspacing="0" cellpadding="0">
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr valign="bottom">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
    
                                                        <td width="200" bgcolor="#FFFFFF" align="left">
														';
														if($nouveau == "true" && ($type == $qType || $type == "") && ($profil == $qProfil || $profil == "" || $qProfil == ""))
                                                        	echo '<font face="Arial, Helvetica, sans-serif" size="-2" color="#FF0000"><b>NEW OPENING</b></font><br>';
														if(($type == $qType || $type == "") && ($profil == $qProfil || $profil == "" || $qProfil == ""))
														echo
														'
														<a href=javascript:OpenWindow("'.$code.'")>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>'.$titre.'</b></font></a><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$date.'</font><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$type.'</font>
                                                        </td>
														
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="125" bgcolor="#FFFFFF" align="left">
														<a href=javascript:OpenWindow("'.$code.'")>
															<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066" class="thiscontent4">Cliquez ici pour une <br>description détaillée.</font>
                                                        </a>
														</td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>																									
                                                </table>
                                                </td></tr>
                                                </table>
                                            </td></tr>
                                    </table>';
										}
                                    ?>
                                    
                              </td>
                                <td valign="top" align="center" width="190" padding="20" background="../images/bg-1.gif"><div align="left">
                                <p class="style2">Please choose a category corresponding to your profile:</p>
                                <p class="style3">
                                  <a href="emplois.php?type=<?php echo $qType; ?>">All profiles</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=Gestion">Management</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=Développement">Developpement</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=Technique">Technical</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=Divers">Others</a>
                                </p>
                                  <br />
                                  <br />
                                  <img src="../images/carriere.jpg" width="185" height="307" border="1"></img>
                                </div></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr size="1"></hr></td>
                            </tr>
                            <tr class="th">
                                <td align="right" valign="top"></td>
                                <td align="left" valign="top"><font size="-2" face="Arial, Helvetica, sans-serif">© Groupe conseil OSI inc. 2005 <br></br>Tous droits réservés</font></td>
                            </tr>
                        </table>						
                    </td>
                    <td background="../images/bordure_droite.jpg" width="10">&#160;</td>
                    <td background="../images/bg.gif">&#160;</td>
                </tr>
            </table>
    </body>
</html>
