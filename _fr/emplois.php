<?php
	$qType = $_GET['type'];
	if($qType == '')
	{
		$qType = 'Contractuel';
	}
	$qProfil = $_GET['profil'];
	
	switch($qProfil)
	{
		case "1":
		$aProfil = "Gestion";
		break;
		case "2":
		$aProfil = "D�veloppement";
		break;
		case "3":
		$aProfil = "Infrastructure";
		break;
		case "4":
		$aProfil = "T�l�com / Ing�nierie";
		break;
		case "5":
		$aProfil = "Progiciel de gestion int�gr� (ERP)";
		break;
		case "6":
		$aProfil = "Autres expertises";
		break;
	}
										
    $xml = new DomDocument('1.0'); 
    $xml->load('emplois.xml'); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include("lib/inc/header.html");?>
        <style type="text/css">
            .thiscontent3
            {
                line-height:14px;
            }
			.thiscontent4
            {
                line-height:10px;
            }
            html,body 
            {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
                height:100%;
            }
            table.th
            {
                height: 100%;
            }
       		.style2 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
			}
       		.style3 
			{	
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}			
			.style4 {color: #CCCCCC}
			
			a
			{
			color:gray;
			text-decoration:none;
			}
			a:visited {color:black}
			a:active  {color:mediumblue}
			a:hover   
			{
			color:mediumblue;
			text-decoration:underline;
			}
        </style>
    </head>	
    
    <body bottommargin="0" topmargin="0" marginheight="0" marginwidth="0">
            <table class="th" height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
                <?php include("lib/inc/flashnav.html");?>
                
                <tr class="th">
                    <td background="../images/bg.gif">&#160;</td>
                    <td background="../images/bordure_gauche.jpg" width="10">&#160;</td>
                    <td width="750" valign="top">
                        <table height="100%" width="749" border="0" cellpadding="10" cellspacing="0">
    
    
                            <tr valign="top">
                              <td align="center">
	                                
                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>Voici nos postes actuellement ouverts</b></font>
                                    <br />
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><?php echo "$qType - ".(($qProfil == '') ? 'Tous les profils' : $aProfil)?></font><br>
                                    </br>
                                    
                                    <?php
										$emplois = $xml->getElementsByTagName("item");
										
										foreach($emplois as $itemEmploiNode)
										{
											//$titre = $itemEmploiNode->getElementsByTagName('titre')->item(0);
											$titres = $itemEmploiNode->getElementsByTagName("titre");
											$titre = $titres->item(0)->nodeValue;
											$titre = iconv("UTF-8", "ISO-8859-1", $titre);
											
											$nouveaux = $itemEmploiNode->getElementsByTagName("nouveau");
											$nouveau = $nouveaux->item(0)->nodeValue;
									
											$categories = $itemEmploiNode->getElementsByTagName("category");
											$category = $categories->item(0)->nodeValue;
											
											$dates = $itemEmploiNode->getElementsByTagName("date");
											$date = $dates->item(0)->nodeValue;
											$date = iconv("UTF-8", "ISO-8859-1", $date);
									
											$localisations = $itemEmploiNode->getElementsByTagName("localisation");
											$localisation = $localisations->item(0)->nodeValue;
											$localisation = iconv("UTF-8", "ISO-8859-1", $localisation);
									
											$durees = $itemEmploiNode->getElementsByTagName("duree");
											$duree = $durees->item(0)->nodeValue;
											$duree = iconv("UTF-8", "ISO-8859-1", $duree);
									
											$codes = $itemEmploiNode->getElementsByTagName("code");
											$code = $codes->item(0)->nodeValue;
											$code = iconv("UTF-8", "ISO-8859-1", $code);
									
											$descriptions = $itemEmploiNode->getElementsByTagName("description");
											$texts = $descriptions->item(0)->getElementsByTagName("text");
											$description = $texts->item(0)->nodeValue;
											$description = iconv("UTF-8", "ISO-8859-1", $description);
											
											$types = $itemEmploiNode->getElementsByTagName("type");
											$type = $types->item(0)->nodeValue;
											
											$profils = $itemEmploiNode->getElementsByTagName("profil");
											$profil = $profils->item(0)->nodeValue;
											$profil = iconv("UTF-8", "ISO-8859-1", $profil);
									
											//echo "$titre"." "."$nouveau"." "."$category"." "."$date"." "."$localisation"." "."$duree"." "."$code"." "."$description"." "."$type<br>";
											//echo 'C '.$profil.' et '.$aProfil.' = '.($profil == $aProfil).' E '.$qType.' et '.$type.' = '.($qType == $type).' R '.(($profil == $aProfil) && ($qType == $type)).' <br/>';
											//echo $profil."=".$aProfil."----------------";
									if(($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
                                    echo '
                                    <table cellspacing="0" cellpadding="5" bordercolor="#CCCCCC" border="0">
                                            <tr><td>
                                                <table width="400" border="1" cellpadding="0" cellspacing="0" bordercolor="#000066">
                                                <tr><td>
                                                <table width="398" border="0" cellspacing="0" cellpadding="0">
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr valign="bottom">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
    
                                                        <td width="200" bgcolor="#FFFFFF" align="left">
														';
														if($nouveau == "true" && ($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
                                                        	echo '<font face="Arial, Helvetica, sans-serif" size="-2" color="#FF0000"><b>NOUVEAU POSTE</b></font><br>';
														if(($type == $qType || $type == "") && ($profil == $aProfil || $profil == "" || $qProfil == ""))
														echo
														'
														<a href=javascript:OpenWindow("'.$code.'")>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>'.$titre.'</b></font></a><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$date.'</font><br>
                                                        <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">'.$type.'</font>
                                                        </td>
														
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="125" bgcolor="#FFFFFF" align="left">
														<a href=javascript:OpenWindow("'.$code.'")>
															<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066" class="thiscontent4">Cliquez ici pour une <br>description d�taill�e.</font>
                                                        </a>
														</td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>
                                                    <tr height="2">
                                                        <td width="15" bgcolor="#000066"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>
                                                        <td width="200" bgcolor="#FFFFFF"></td>
                                                        <td width="15" bgcolor="#FFFFFF"></td>	
                                                        <td width="125" bgcolor="#FFFFFF"></td>
                                                        <td bgcolor="#000066"></td>					
                                                    </tr>																									
                                                </table>
                                                </td></tr>
                                                </table>
                                            </td></tr>
                                    </table>';
										}
                                    ?>
                                    <br />
                                    <font face="Arial, Helvetica, sans-serif" size="-1" color="#000066">Veuillez prendre note que l'entreprise applique un programme d'acc&egrave;s &agrave; l'&eacute;galit&eacute; et nous invitons tous les membres des groupes vis&eacute;s, femmes, minorit&eacute;s visibles, handicap&eacute;s et autochtones &agrave; postuler sur ce poste. L'utilisation du genre masculin n'a que pour objectif d'all&eacute;ger le texte.</font>
                                    <br />
                              </td>
                                <td valign="top" align="center" width="190" padding="20" background="../images/bg-1.gif"><div align="left">
                                <p class="style2">Veuillez choisir une cat&eacute;gorie correspondant &agrave; votre profil d'emploi :</p>
                                <p class="style3">
                                  <a href="emplois.php?type=<?php echo $qType; ?>">Tous les profils</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=1">Gestion</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=2">D&eacute;veloppement</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=3">Infrastructure</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=4">T&eacute;l&eacute;com / Ing&eacute;nierie</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=5">Progiciel de gestion int&eacute;gr&eacute; (ERP)</a><br />
                                  <a href="emplois.php?type=<?php echo $qType; ?>&profil=6">Autres expertises</a>
                                </p>
                                  <br />
                                  <br />
                                  <img src="../images/carriere.jpg" width="185" height="307" border="1"></img>
                                </div></td>
                            </tr>
                        </table>						
                    </td>
                    <td background="../images/bordure_droite.jpg" width="10">&#160;</td>
                    <td background="../images/bg.gif">&#160;</td>
                </tr>
                <?php include("lib/inc/footer.html");?>
            </table>
    </body>
</html>
