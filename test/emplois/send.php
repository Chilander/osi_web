<?php
	$qId = $_GET['id'];
										
    $xml = new DomDocument('1.0'); 
    $xml->load('C2006-2610-11.xml');
	$emploi = $xml->getElementsByTagName("emplois");

	$code = $emploi->item(0)->getElementsByTagName("code")->item(0)->nodeValue;
	$titre = $emploi->item(0)->getElementsByTagName("titre")->item(0)->nodeValue;
	$type = $emploi->item(0)->getElementsByTagName("type")->item(0)->nodeValue;
	$localisation = $emploi->item(0)->getElementsByTagName("localisation")->item(0)->nodeValue;
	$duree = $emploi->item(0)->getElementsByTagName("duree")->item(0)->nodeValue;
	
	$descriptions = $emploi->item(0)->getElementsByTagName("description");
	$description = "";
	
	foreach($descriptions as $desc)
	{
		$description = $description.$desc->getElementsByTagName("text")->item(0)->nodeValue." ";
	}
	
	$expertises = $emploi->item(0)->getElementsByTagName("expertise");
	$expertise = "";
	foreach($expertises as $exp)
	{
		$expertise = $expertise.$exp->getElementsByTagName("text")->item(0)->nodeValue.". ";
	}
	
	echo("descrpition = ".$description);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="expires" content="0"></meta>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"></META>
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"></META>
		<title>Poste - <xsl:value-of select="titre"/></title>
			<style type="text/css">
				.content3
				{
					line-height:14px;
				}
				.style1 {
					font-family: Arial, Helvetica, sans-serif;
					font-weight: bold;
					color: #0C2D83;
				}
				.style2 {color: #0C2D83}
				.style3 {
					font-family: Arial, Helvetica, sans-serif;
					color: #FFFFFF;
					font-size: 12px;
					font-weight: bold;
				}
				.style4 {
					font-family: Arial, Helvetica, sans-serif;
					font-size: 12px;
					color: #0C2D83;
				}
				.style5 {
					font-family: Arial, Helvetica, sans-serif;
					color: #000000;
					font-size: 10px;
					font-weight: bold;
				}
				.style6 {
					font-family: Arial, Helvetica, sans-serif;
					font-size: 20px;
					color: #000000;
				}

				html,body 
				{
					margin-left: 0px;
					margin-top: 0px;
					margin-right: 0px;
					margin-bottom: 0px;
					height: 100%;
				}
				.th
				{
					height: 100%;
				}
		</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

	<body>
	  <table class="th" height="100%" width="600" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td valign="top">
				<table class="th" height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
<!--Rang 1 -->
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20">&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 2 -->
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" height="76"><img src="../images/osi_logo.gif" width="175" height="76"></img></td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 5 -->
					<tr>
					  <td width="176" height="19" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">
						<form name="form1" method="post" action="fowardconfirmation.php" ENCTYPE="multipart/form-data">
						  <p class="style4"><b>Vous avez choisi d'envoyer le poste suivant : </b><br></br></p>
                          <p class="style6"><b><?php echo $titre;?></b><br></br></p>
                          
							<p class="style4"> Courriel du destinataire<br></br>
							<input type="text" name="to" style="width:240px; font-size:12px" /><br></br><br></br>Votre nom<br />
							<input type="text" name="name" style="width:240px; font-size:12px" />
							<br />
   							<br />Votre courriel<br />
                            </br>
                            <input type="text" name="from" style="width:240px; font-size:12px" />
                            <input type="hidden" name="titre" value="<?php echo $titre; ?>" />
                            <input type="hidden" name="code" value="<?php echo $code; ?>" />
							<input type="hidden" name="type" value="<?php echo $type; ?>" />
                            <input type="hidden" name="localisation" value="<?php echo $localisation; ?>" />
                            <input type="hidden" name="duree" value="<?php echo $duree; ?>" />
                            <input type="hidden" name="description" value="<?php echo $description; ?>" />
                            <input type="hidden" name="expertise" value="<?php echo $expertise; ?>" />
                                                        
							</p>
                            <br />
                            <button type="submit" title="soumettre">Soumettre</button>

					    </form>
						</td>
					  <td width="20">&#160;</td>
					</tr>

					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" class="style4">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
					<tr class="th">
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450"></td>
					  <td width="20">&#160;</td>
					</tr>
				</table>
			</td>
		</tr>
	  </table>
<body>
</body>
</html>
