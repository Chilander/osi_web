<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-public="-//w3c//DTD HTML 4.01 Transitional//EN"></xsl:output>
<xsl:template match="/">
<xsl:for-each select="emplois">
	<html>
	<head>
	<meta http-equiv="expires" content="0"></meta>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"></META>
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"></META>
		<title>Poste - <xsl:value-of select="titre"/></title>
			<style type="text/css">
				.content3
				{
					line-height:14px;
				}
				.style1 {
					font-family: Arial, Helvetica, sans-serif;
					font-weight: bold;
					color: #0C2D83;
				}
				.style2 {color: #0C2D83}
				.style3 {
					font-family: Arial, Helvetica, sans-serif;
					color: #FFFFFF;
					font-size: 12px;
					font-weight: bold;
				}
				.style4 {
					font-family: Arial, Helvetica, sans-serif;
					font-size: 12px;
					color: #0C2D83;
				}
				.style5 {
					font-family: Arial, Helvetica, sans-serif;
					color: #000000;
					font-size: 10px;
					font-weight: bold;
				}
				html,body 
				{
					margin-left: 0px;
					margin-top: 0px;
					margin-right: 0px;
					margin-bottom: 0px;
					height: 100%;
				}
				.th
				{
					height: 100%;
				}
		</style>
	</head>

	<body>
	  <table class="th" height="100%" width="600" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td valign="top">
				<table class="th" height="100%" width="100%" border="0" cellpadding="0" cellspacing="0">
<!--Rang 1 -->
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20" background="../images/bg-osi.gif">&#160;</td>
					  <td width="20">&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 2 -->
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" height="76"><img src="../images/osi_logo.gif" width="175" height="76"></img></td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 3 -->					
					<tr>
					  <td width="176" height="50" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" valign="top" align="right">
						<xsl:element name="a">
						<xsl:attribute name="href">javascript:window.print()</xsl:attribute>
						<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066">Cliquez ici pour imprimer la page.</font>
						</xsl:element>
                        <br />
 						<xsl:element name="a">
						<xsl:attribute name="href">send.php?id=<xsl:value-of select="code"/></xsl:attribute>
						<font face="Arial, Helvetica, sans-serif" size="-2" color="#000066">Envoyer � un ami</font>
						</xsl:element>                       
					  </td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 4 -->					
					<tr>
					  <td width="176" height="19" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td><span class="style2"></span></td>
					  <td width="450"><span class="style1"><xsl:value-of select="titre"/></span><br></br>
					  <span class="style4"><xsl:value-of select="code"/></span>
					  </td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 5 -->
					<tr>
					  <td width="176" height="19" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
<!--Rang 6 -->
					<tr valign="top">
					  <td width="176" height="21" align="right" background="../images/bg-osi.gif"><div align="right">
						<p class="style3"><xsl:value-of select="date"/><br></br>
						  Poste <xsl:value-of select="type"/><br></br>
							<xsl:if test="type = 'Contractuel'">
								<xsl:value-of select="duree"/><br></br>
						   </xsl:if>						  
						  <xsl:value-of select="localisation"/>
						</p>
						</div></td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">
							<table class="th" height="100%" width="450" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<span class="style4">
											<xsl:for-each select="description">
											   <xsl:value-of select="text"/><br></br>
											</xsl:for-each>
										</span><br></br>
									</td>
								</tr>
								<tr>
									<td>
										<span class="style4">
											<ul>
												<xsl:for-each select="expertise">
												   <li><xsl:value-of select="text"/></li>
												</xsl:for-each>
											  </ul>
										</span>
									</td>
								</tr>
	        					<xsl:if test="technologie != ''">
								<tr>
									<td><span  class="style4">
										<b>Exigences technologiques : </b><br></br>
										<blockquote>
											<xsl:for-each select="technologie">
											   <xsl:value-of select="text"/><br></br>
											</xsl:for-each>										  
										</blockquote>
									</span></td>
								</tr>
 						       </xsl:if>
							</table>
					  </td>
					  <td width="20">&#160;</td>
					</tr>
					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450">
						<form name="form1" method="post" action="confirmation.php" ENCTYPE="multipart/form-data">
							<p class="style4"><b>Vous pouvez postuler pour ce poste en nous envoyant votre CV.</b><br></br></p>

							<p class="style4">Votre nom<br></br>
							<input type="text" name="nom" style="width:240px; font-size:12px"></input><br></br><br></br></p>
							
							<p class="style4">Votre adresse courriel<br></br>
							<input type="text" name="courriel" style="width:240px; font-size:12px"></input><br></br><br></br></p>

							<p class="style4">Veuillez attacher votre C.V. (format Word, RTF ou PDF, max. 1MB)<br></br>
							<input type="file" name="fileatt" style="height:20px; width:240px; font-size:12px"> </input>
							<input type="submit" value="Envoyer" style="height:20px; width:80px; font-size:11px"> </input></p>
						<xsl:element name="input">
						<xsl:attribute name="type">hidden</xsl:attribute>
						<xsl:attribute name="name">titre</xsl:attribute>
						<xsl:attribute name="value">Emploi <xsl:value-of select="code"/> - <xsl:value-of select="titre"/></xsl:attribute>
						</xsl:element>
						</form>
						<p class="style5">
						Une fois re�u, votre C.V. sera achemin� vers notre responsable du recrutement correspondant au poste auquel vous avez appliqu�. Veuillez noter que nous traitons tous les dossiers re�us en respectant nos politiques strictes de confidentialit�.</p>
					  </td>
					  <td width="20">&#160;</td>
					</tr>

					<tr>
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450" class="style4">&#160;</td>
					  <td width="20">&#160;</td>
					</tr>
					<tr class="th">
					  <td width="176" valign="top" background="../images/bg-osi.gif">&#160;</td>
					  <td background="../images/bg-osi.gif">&#160;</td>
					  <td>&#160;</td>
					  <td width="450"></td>
					  <td width="20">&#160;</td>
					</tr>
				</table>
			</td>
		</tr>
	  </table>

	</body>
	</html>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>