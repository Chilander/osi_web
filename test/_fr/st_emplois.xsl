<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-public="-//w3c//DTD HTML 4.01 Transitional//EN"></xsl:output>

<xsl:template match="/">
	<html>
		<head>
			<title>Groupe conseil OSI - profils recherch�s</title>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></meta>
			<script type="text/javascript" src="../script.js"></script>
            <script type="text/javascript" src="../Scripts/AC_RunActiveContent.js" ></script>
			<style type="text/css">
				.content3
				{
					line-height:14px;
				}
				html,body 
				{
					margin-left: 0px;
					margin-top: 0px;
					margin-right: 0px;
					margin-bottom: 0px;
					height:100%;
				}
				table.th
				{
					height: 100%;
				}
			</style>
		    
		</head>	
	
		<body bottommargin="0" topmargin="0" marginheight="0" marginwidth="0">
			<table class="th" height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="235">
					<td background="../images/bg.jpg">&#160;</td>
					<td background="../images/bordure_gauche_haut.jpg" width="10">&#160;</td>
					<td width="750">
					<embed width="750" height="235" src="flashemploi.swf" quality="high" pluginspace="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" />
                    <script type="text/javascript">
					<![CDATA[
						function checkType()
						{
							var is_input = document.URL.indexOf('?');
							var param;
							var data;
							if(is_input != -1)
							{
								param = document.URL.slice(is_input+1);
								data = param.split("&");
								setType(data[0]);
								setCategory(data[1]);
								
								alert(getType());
							}
						}
						checkType();
					]]>
                    </script>

                    <!--
					<script type="text/javascript">
                        AC_FL_RunContent( 
                            'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0',
                            'width','750',
                            'height','235',
                            'src','flashemploi',
                            'quality','high',
                            'pluginspage','http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash',
                            'movie','flashemploi' ); //end AC code
                    </script>
                    -->
					</td>
					<td background="../images/bordure_droite_haut.jpg" width="10">&#160;</td>
					<td background="../images/bg.jpg">&#160;</td>
				</tr>
				
				<tr class="th">
					<td background="../images/bg.gif">&#160;</td>
					<td background="../images/bordure_gauche.jpg" width="10">&#160;</td>
					<td width="750" valign="top">
						<table height="100%" width="749" border="0" cellpadding="10" cellspacing="0">

							
							<tr valign="top">
								<td align="center">
									<font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b>Voici nos postes actuellement ouverts</b></font>
									<br></br>
									
									<table cellspacing="0" cellpadding="5" bordercolor="#CCCCCC" border="0">
										<xsl:for-each select="emplois/item">
                                        	<xsl:if test="category='gestion'">
											<tr><td>
												<table width="400" border="1" cellpadding="0" cellspacing="0" bordercolor="#000066">
												<tr><td>
												<table width="398" border="0" cellspacing="0" cellpadding="0">
													<tr height="2">
														<td width="15" bgcolor="#000066"></td>
														<td width="15" bgcolor="#FFFFFF"></td>
														<td width="15" bgcolor="#FFFFFF"></td>
														<td width="150" bgcolor="#FFFFFF"></td>
														<td width="15" bgcolor="#FFFFFF"></td>	
														<td width="150" bgcolor="#FFFFFF"></td>
														<td bgcolor="#000066"></td>					
													</tr>
													<tr valign="bottom">
														<td width="15" bgcolor="#000066"></td>
														<td width="15" bgcolor="#FFFFFF"></td>
														<td width="15" bgcolor="#FFFFFF"></td>

														<td width="150" bgcolor="#FFFFFF">
															<xsl:element name="a">
															<xsl:attribute name="href">javascript:OpenWindow("<xsl:value-of select="code"/>")</xsl:attribute>
																<font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><b><xsl:value-of select="titre"/></b></font>
															</xsl:element>
															<br></br>
															<font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><xsl:value-of select="date"/></font>
															<br></br>
															<font face="Arial, Helvetica, sans-serif" size="-1" color="#000066"><xsl:value-of select="type"/></font>
														</td>															
														<td width="15" bgcolor="#FFFFFF"></td>	
														<td width="150" bgcolor="#FFFFFF">
															<xsl:element name="a">
															<xsl:attribute name="href">javascript:OpenWindow("<xsl:value-of select="code"/>")</xsl:attribute>
																<font class="content3" face="Arial, Helvetica, sans-serif" size="-2" color="#000066">Cliquez ici pour une <br></br>description d�taill�e.</font>
															</xsl:element>
														</td>
														<td bgcolor="#000066"></td>					
													</tr>
													<tr height="2">
														<td width="15" bgcolor="#000066"></td>
														<td width="15" bgcolor="#FFFFFF"></td>
														<td width="15" bgcolor="#FFFFFF"></td>
														<td width="150" bgcolor="#FFFFFF"></td>
														<td width="15" bgcolor="#FFFFFF"></td>	
														<td width="150" bgcolor="#FFFFFF"></td>
														<td bgcolor="#000066"></td>					
													</tr>																									
												</table>
												</td></tr>
												</table>
											</td></tr>
                                           	</xsl:if>
										</xsl:for-each>
										
										
																			
									</table>
								</td>
								<td valign="top" align="center" width="210" padding="20" background="../images/bg-1.gif">
									<img src="../images/carriere.jpg" width="185" height="307" border="1"></img>
								</td>
							</tr>
							<tr>
								<td colspan="2"><hr size="1"></hr></td>
							</tr>
							<tr class="th">
								<td align="right" valign="top"></td>
								<td align="left" valign="top"><font size="-2" face="Arial, Helvetica, sans-serif">� Groupe conseil OSI inc. 2005 <br></br>Tous droits r�serv�s</font></td>
							</tr>
						</table>						
					</td>
					<td background="../images/bordure_droite.jpg" width="10">&#160;</td>
					<td background="../images/bg.gif">&#160;</td>
				</tr>
			</table>


		</body>
	</html>
</xsl:template>
</xsl:stylesheet>