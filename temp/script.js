function enregistrerCookie(valeur, expireDate) {
	document.cookie = "lang=" + escape(valeur) + "; expires=" + expireDate + "; path=/";
}
function makeCookieEn() {
	result = "en";
	var unJour = 24 * 60 * 60 * 1000; //variable en milliseconde d'un jour
	var expDate = new Date(); //variable objet de date
	expDate.setTime (expDate.getTime() + (999*unJour)); //calculer la date d'expiration
	enregistrerCookie(result, expDate.toGMTString()); //appel de la fonction
}
function makeCookieFr() {
	result = "fr";
	var unJour = 24 * 60 * 60 * 1000; //variable en milliseconde d'un jour
	var expDate = new Date(); //variable objet de date
	expDate.setTime (expDate.getTime() + (999*unJour)); //calculer la date d'expiration
	enregistrerCookie(result, expDate.toGMTString()); //appel de la fonction
}
function readCookie() {
	return document.cookie;
}
function english() {
	var thisURL = document.URL;
	var newURL = thisURL.replace("_fr", "_en");
	makeCookieEn();
	location.href=newURL;
}
function francais() {
	var thisURL = document.URL;
	var newURL = thisURL.replace("_en", "_fr");
	makeCookieFr();
	location.href=newURL;
}
function openNouvelles(dateNouvelle){
	fichier = "nouvelles/" + dateNouvelle + ".htm";
	myWindow = window.open(fichier, "NouvellesGroupeConseilOSI", "width=760,height=700,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=yes,status=no,left=2,top=100");
}
function OpenWindow(job){
	fichier ="../emplois/" + job + ".xml";
	myWindow = window.open(fichier, "EmploiGroupeConseilOSI", "width=650,height=700,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=yes,status=no,location=no,left=2,top=100");
	myWindow.focus();
}
function openImage(img) {
	myWindow = window.open("../photo/" + img, "OSI", "width=680,height=480,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=no,status=no,location=no,left=2,top=100");
}
function openImageVer(img) {
	myWindow = window.open("../photo/" + img, "OSI", "width=480,height=640,location=no,toobar=no,menubar=no,scrollbars=yes,resizable=no,status=no,location=no,left=2,top=100");
}